const webpack = require('webpack')

const webpackMerge = require('webpack-merge')
const baseWebPackConfig = require('./webpack.base.config')

module.exports = webpackMerge(baseWebPackConfig, {
  devtool: 'null',
})